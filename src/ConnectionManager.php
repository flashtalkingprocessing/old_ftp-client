<?php namespace Flashtalking\Ftp;

use Flashtalking\Ftp\Exception\FtpException;

class ConnectionManager
{
    protected $connections = array();

    protected $connectionSettings = array();

    protected $connectionRetryAttempts = 5;

    public function __construct(array $connectionSettings)
    {
        $this->connectionSettings = $connectionSettings;
    }

    public function getConnection($connectionID)
    {
        if (!isset($this->connections[$connectionID])) {
            $this->connections[$connectionID] = $this->makeConnection($connectionID);
        }

        $this->connections[$connectionID]->connect();

        return $this->connections[$connectionID];
    }

    private function makeConnection($connectionID)
    {
        if (!isset($this->connectionSettings[$connectionID])) {
            throw new FtpException("Log source [$connectionID] does not exist");
        }

        $config = $this->connectionSettings[$connectionID];

        return new Connection($config['host'], $config['username'], $config['password'], $this->connectionRetryAttempts, $config['ssl']);
    }
}
