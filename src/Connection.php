<?php namespace Flashtalking\Ftp;

use Exception;
use Flashtalking\Ftp\Exception\FtpException;
use Flashtalking\Ftp\Exception\FtpConnectionException;

class Connection
{
    const MAX_DOWNLOAD_ATTEMPTS = 2;

    protected $server_connection;

    private $server;

    private $user;

    private $pass;

    private $retry;

    private $use_ssl;

    public function __construct($server, $user, $pass, $retry = 5, $use_ssl = false)
    {
        $this->server = $server;

        $this->user = $user;

        $this->pass = $pass;

        $this->retry = $retry;

        $this->use_ssl = $use_ssl;

        $this->connect();
    }

    public function connect()
    {
        if(!is_null($this->server_connection))
        {
            return;
        }

        list($host, $port) = $this->parseDsn($this->server);

        $retry = $this->retry;

        while (!($connection = ($this->use_ssl ? ftp_ssl_connect($host, $port) : ftp_connect($host, $port))) && $retry-- > 0) {
            sleep(10);
        }

        if (!$connection) {
            throw new FtpConnectionException('Failed to connect to FTP server: '. $this->server . ' after ' . $this->retry . ' attempts.');
        }

        $login = ftp_login($connection, $this->user, $this->pass);

        if (!$login) {
            throw new FtpConnectionException("Failed to login to ftp server using {$this->user}, {$this->pass}");
        }

        ftp_pasv($connection, true);

        $this->server_connection = $connection;
    }

    protected function parseDsn($dsn)
    {
        $parts = explode(':', $dsn, 2);

        return array(
            $parts[0],
            isset($parts[1]) ? $parts[1] : 21
        );
    }

    /**
     * Change the directory in the ftp connection
     */

    public function changeDirectory($swap_directory)
    {
        try {
            ftp_chdir($this->server_connection, $swap_directory);
        } catch (Exception $ex) {
            throw new FtpException("Failed to change directory ",0,$ex);
        }

        return $this;
    }

    /**
     * Recursively make a directory on the for server
     *
     * @param $directoryPath
     * @return $this
     */
    public function makeDirectory($directoryPath)
    {
        $pathParts = explode('/',$directoryPath);

        $dirs = array();

        while ($pop = array_shift($pathParts)) {
            $dirs[] = $pop;

            $dir = implode('/', $dirs);

            try {
                ftp_mkdir($this->server_connection, $dir);
            } catch (\Exception $e) {
                continue;
            }
        }

        return $this;
    }

    /**
     * List directory over FTP
     *
     * @param  string $directory
     * @param  int    $timeout
     * @return array
     */
    public function listDirectory($directory, $timeout = 900)
    {
        $old = $this->setTimeout($timeout);

        $list = ftp_nlist($this->server_connection, $directory);

        $this->setTimeout($old);

        return $list === false ? array() : $list;
    }

    /**
     * Get the file size in bytes from the passed filename
     */

    public function fileSize($filename)
    {
        return ftp_size($this->server_connection, $filename);
    }

    /**
     * Move the log file to a local directory for processing
     *
     * @param $filename
     * @param $destination
     * @return $this
     * @throws Exception
     */
    public function downloadFile($filename, $destination)
    {
        return ftp_get($this->server_connection, $destination, $filename, FTP_BINARY);
    }

    /**
     * Set the timeout for all subsequent remote operations
     *
     * @param $timeout
     * @return int
     */
    public function setTimeout($timeout)
    {
        $old = $this->getTimeout();

        ftp_set_option($this->server_connection, FTP_TIMEOUT_SEC, $timeout);

        return $old;
    }

    /**
     * Get the timeout for remote operations
     *
     * @return int $timeout
     */
    public function getTimeout()
    {
        return ftp_get_option($this->server_connection, FTP_TIMEOUT_SEC);
    }

    /**
     * Move the log file to a local directory for processing
     *
     * @param $filename
     * @param $destination
     * @return $this
     */
    public function uploadFile($filename, $destination)
    {
        ftp_put($this->server_connection, $destination, $filename, FTP_BINARY);

        return $this;
    }
    
    /**
     * Write data directly to the ftp server with the given filename
     *
     * @param string $data
     * @param $destination
     * @return $this
     */
    public function write($data, $destination)
    {
        $file = fopen("php://memory", "r+");

        fwrite($file, $data);

        rewind($file);

        ftp_fput($this->server_connection, $destination, $file, FTP_BINARY);

        return $this;
    }
    
    /**
     * Read data directly from the ftp server with the given filename
     *
     * @param $filename
     * @return $this
     */
    public function read($filename)
    {
        $file = fopen("php://memory", "r+");

        ftp_fget($this->server_connection, $file, $filename, FTP_BINARY);

        $len = ftell($file);

        fseek($file, 0);

        return fread($file, $len);
    }

    /**
     *
     */
    public function disconnect()
    {
        if(!is_null($this->server_connection))
        {
            ftp_close($this->server_connection);
        }

        $this->server_connection = null;
    }

    /**
     *
     */
    public function __destruct()
    {
        $this->disconnect();
    }
}
